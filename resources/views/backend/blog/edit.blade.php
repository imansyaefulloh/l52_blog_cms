@extends('layouts.backend.main')
@section('title', 'MyBlog | Edit post')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Blog
        <small>Edit post</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/home') }}">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{ route('backend.blog.index') }}">Blog</a>
            </li>
            <li class="active">
                Edit post
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            {!! Form::model($post, [
                'method' => 'PUT',
                'route' => ['backend.blog.update', $post->id],
                'files' => true,
                'id'    => 'blog-post'
                ]) !!}
                
                @include('backend.blog.form')

            {!! Form::close() !!}
        </div>

        <!-- ./row -->
    </section>
    <!-- /.content -->
</div>
@endsection