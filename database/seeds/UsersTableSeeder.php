<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset the users table
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();

        $faker = Factory::create();

        // generate 3 users/author
        DB::table('users')->insert([
            [
                'name' => "Iman Syaefulloh",
                'slug' => 'iman-syaefulloh',
                'email' => "imansyaefulloh@gmail.com",
                'password' => bcrypt('secret'),
                'bio' => $faker->text(rand(250, 300))
            ],
            [
                'name' => "John Doe",
                'slug' => 'john-doe',
                'email' => "johndoe@gmail.com",
                'password' => bcrypt('secret'),
                'bio' => $faker->text(rand(250, 300))
            ],
            [
                'name' => "Jane Doe",
                'slug' => 'jane-doe',
                'email' => "janedoe@gmail.com",
                'password' => bcrypt('secret'),
                'bio' => $faker->text(rand(250, 300))
            ]
        ]);
    }
}
