<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Post;
use App\User;
use App\Category;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    protected $limit = 5;
    protected $commentsLimit = 1;
    
    public function index()
    {
        $posts = Post::with('author', 'tags', 'category', 'comments')
                    ->latestFirst()
                    ->published()
                    ->filter(request()->only(['term', 'year', 'month']))
                    ->paginate($this->limit);

        return view('blog.index', compact('posts', 'categories'));
    }
 
    public function show(Post $post)
    {
        $post->increment('view_count');

        $postComments = $post->comments()->paginate(5);
        
        return view('blog.show', compact('post', 'postComments'));
    }

    public function category(Category $category)
    {
        $categoryName = $category->title;

        $posts = $category->posts()
                    ->with('author', 'tags', 'category', 'comments')
                    ->latestFirst()
                    ->published()
                    ->paginate($this->commentsLimit);

        return view('blog.index', compact('posts', 'categoryName'));
    }

    public function author(User $author)
    {
        $authorName = $author->name;

        $posts = $author->posts()
                    ->with('author', 'tags', 'category', 'comments')
                    ->latestFirst()
                    ->published()
                    ->paginate($this->limit);

        return view('blog.index', compact('posts', 'authorName'));
    }

    public function tag(Tag $tag)
    {
        $tagName = $tag->title;

        $posts = $tag->posts()
                    ->with('author', 'tags', 'category', 'comments')
                    ->latestFirst()
                    ->published()
                    ->paginate($this->limit);

        return view('blog.index', compact('posts', 'tagName'));
    }
}
